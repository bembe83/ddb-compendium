
Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		Babele.get().register({
			module: 'ddb-compendium',
			lang: 'it',
			dir: 'compendium-it'
		});
	}
});